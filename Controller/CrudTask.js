var db = require('../Db/dbConnection'); //reference of dbconnection.js
var CrudTask = {
    insertAuthToken: function (token, callback) {
        return db.query("Insert into token_generate (generate_token) values (?)", [token], callback);
    },
    validateAuth: function (token, callback) {
        return db.query("SELECT count(*) as counts FROM nuture_co.token_generate where generate_token=?", [token], callback);
    },
    insertData: function (Task, callback) {
        return db.query("Insert into nuture_currencyprice (current_price,currency_code,decription) values (?,?,?)", [Task.current_price, Task.currency_code, Task.decription], callback);
    },
    getCommodityList: function (id, callback) {
        return db.query("SELECT * FROM nuture_co.nuture_currencyprice where id = ?", [id], callback);
    },
};
module.exports = CrudTask;