var express = require('express')
var cors = require('cors')
var app = express()
app.use(cors());
var bodyParser = require('body-parser');
var CrudTask = require('./Route/CrudTask');
const fs = require('fs');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use((req,res,next)=> {
var now = new Date().toString();
var log = `${now}: ${req.method} ${req.url}`;
fs.appendFile('server.log',log + '\n');
next();
});
app.use("/Task",CrudTask);
app.listen(8080, function () {
    console.log('Express server is listening on port 8080');
});