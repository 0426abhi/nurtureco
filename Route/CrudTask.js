var express = require('express');
var router = express.Router();
var CrudTask = require('../Controller/CrudTask');
const { SHA256 } = require('crypto-js');
const jwt = require('jsonwebtoken');

router.get('/generateAuthToken', function (request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    let data = {
        id: 1
    };
    var token = jwt.sign(data, 'nurtureCo');
    CrudTask.insertAuthToken(token, function (error, result) {
        if (error) {
            return response.jsonp({
                status: "error",
                response: error
            });
        }
        else {
            return response.jsonp({
                status: 200,
                authToken: token
            });
        }
    });
});

router.post('/insertCommodity', function (request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    CrudTask.validateAuth(request.header('x-auth'), function (error, result) {
        if (error) {
            return response.jsonp({
                status: "error",
                response: error
            });
        }
        else {
            if (result[0].counts === 1) {
                CrudTask.insertData(request.body, function (error, result) {
                    if (error) {
                        return response.jsonp({
                            status: "error",
                            response: error
                        });
                    }
                    else {
                        return response.jsonp({
                            status: 200,
                            response: result
                        });
                    }
                })
            }
            else {
                return response.jsonp({
                    status: "Authentication Token",
                    response: "Authentication Token is Wrong."
                });
            }
        }
    });
});


router.get('/getCommodity/:id', function (request, response, next) {
    response.header("Access-Control-Allow-Origin", "*");
    response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    CrudTask.validateAuth(request.header('x-auth'), function (error, result) {
        if (error) {
            return response.jsonp({
                status: "error",
                response: error
            });
        }
        else {
            if (result[0].counts === 1) {
                CrudTask.getCommodityList(request.params.id, function (error, result) {
                    if (error) {
                        return response.jsonp({
                            status: "error",
                            response: error
                        });
                    }
                    else {
                        return response.jsonp({
                            status: 200,
                            response: result
                        });
                    }
                });
            } else {
                return response.jsonp({
                    status: "Authentication Token",
                    response: "Authentication Token is Wrong."
                });
            }
        }
    });
});

module.exports = router;