var mysql = require ("mysql");
//development connection
var connection = mysql.createPool({
    host: "localhost",
    user: "root",
    password: "password",
    database: "nuture_co"
});
module.exports = connection;